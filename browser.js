global_dom_idx = 0;

function get_next_idx()
{
	return global_dom_idx++;
}

Object.prototype.keys = function ()
{
  var keys = [];
  for(var i in this) if (this.hasOwnProperty(i))
  {
    keys.push(i);
  }
  return keys;
}
Object.prototype.values = function ()
{
  var keys = [];
  for(var i in this) if (this.hasOwnProperty(i))
  {
    keys.push(this[i]);
  }
  return keys;
}

Browser = {
	getElement : function(selector, idx){
		var el = jQueryGetElement( selector , idx == undefined ? 0 : idx );
		if(el.varName)
			return this._createWrapperElement(el);
		return false;
	},
	log : function( string ){
		log(string);
	},
	setUserAgent : function( useragent ){
		setUserAgent(useragent);
	},
	load : function( url, referrer ){
		loadURI(url, referrer);
	},
	url : function(){
		var loc = getLocation(),
            url = loc['protocol'] + '://' + loc['host'] + loc['path'] + '?' +loc['params'];
        return url;
	},
	msleep : function(msec){
		wait(msec);
	},
	post : function( url, args ){
		postForm(url, args.keys(), args.values());
	},
	_createWrapperElement : function(object){

		return {
			_object : object,
			click : function(){
				return clickElement(this._object);
			},
			write : function( text ){
				if(this.click())
				{
					typeIn(text);
					return true;
				}
				return false;
			},
			_struct : function(){
				var newElement = {'varName':'wacs_found_element_80000_' + get_next_idx()};
		    	newElement.frameName = this._object.frameName;
		    	return newElement;
			},
			parent : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.parentNode', this._object.frameName);
				return Browser._createWrapperElement(newElement);
			},
			prevSibling : function(){
			    var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.previousElementSibling', this._object.frameName);
				return Browser._createWrapperElement(newElement);
			},
			nextSibling : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.nextElementSibling', this._object.frameName);
				return Browser._createWrapperElement(newElement);
			},
			firstChild : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.firstChild', this._object.frameName);
				return Browser._createWrapperElement(newElement);
			},
			lastChild : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.lastChild', this._object.frameName);
				return Browser._createWrapperElement(newElement);
			},
			prev : function(){return this.prevSibling()},
			next : function(){return this.nextSibling()},
			attribute : function(name){
				return getElementAttribute(this._object, name);
			},
			attr : function(name){return this.attribute(name)},
			removeElement : function(){executeJavaScript('$('+this._object.varName+').remove();');},
			remove : function(){this.removeElement()}
		};
	}
}